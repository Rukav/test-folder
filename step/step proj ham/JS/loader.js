// function hideLoader (){
//     document.querySelector('.lds-roller').classList.add('hidden');
// }
// setTimeout(hideLoader,5000);
let loader = document.querySelector('.lds-roller');
let btn = document.querySelector(".loader-btn");
btn.addEventListener("click", () => hideBtn() )

let addition = document.querySelectorAll('.additionally');

function hideBtn() {
    btn.classList.add("hidden");
    loader.classList.add('on-active');
    // addition.add('is-active')
    setTimeout(showBtn,1000)
}

function showBtn () {
    var activeTab = document.querySelector('.art-class-item.is-active');
    var itemsClass = activeTab.getAttribute('data-tab-name');
    loader.classList.remove("on-active");
    addition.forEach(el=>{
        el.classList.remove("additionally");
        el.classList.add("examples-item");
        if (itemsClass === 'All-art') {
            el.classList.remove("hidden");
        } else {
            if (el.classList.contains(itemsClass)) {
                el.classList.remove("hidden");
            }
        }
    })

}


$( document ).ready(function() {
    $('.art-class-item').on('click', function() {
        $('.art-class-item').removeClass('is-active');
        $(this).addClass('is-active');
        var itemsClass = $(this).attr('data-tab-name');
        if (itemsClass === 'All-art') {
            $('.examples-item').removeClass('hidden');
        } else {
            $('.examples-item').each(function () {
                if ($(this).hasClass(itemsClass)) {
                    $(this).removeClass('hidden');
                } else {
                    $(this).addClass('hidden');
                }
            });
        }
    });
});